import { List } from 'immutable';
import Actions from './actions';

// The initial state is not truely our initial state,
// as we need to generate all the deck's card and place them in the deck.
// This is fixed by the RESET action being invoked in the index.js
const initialState = {
  cardsInDeck: List(),
  cardsInHand: List(),
  cardHoverVisible: false
};

// Pretty small reducer... as we only have so little actions, we don't need to split the reducer.
// But (!), should we add more action, reducers should be cut down with their actions and stored with their HOC
// Currently all of these are mostly related to the Control component
export default function(state = initialState, action) {
  switch (action.type) {
    case Actions.RESET:
      return Actions.Reset(state);
    case Actions.SHUFFLE:
      return Actions.Shuffle(state);
    case Actions.DEAL:
      return Actions.DealOneCard(state);
    case Actions.HOVERCHANGED:
      return Actions.HoverChanged(state, action.payload);
    default:
      return state;
  }
}
