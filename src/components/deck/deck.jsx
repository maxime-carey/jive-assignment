import React, { Component } from 'react';
import Card from '../card';
import './deck.css';

/**
 * Deck: Contains the unflipped card
 */
class Deck extends Component
{
    render()
    {
        const {cardsInDeck, showCardsOnHover} = this.props;
        let classes = "deck" + (showCardsOnHover ? " showCardsOnHover" :  "");

        return(
            <div className={classes}>
            {cardsInDeck.reverse().map((c,i) =>  <Card key={i} color={c.color} face={c.face} flipped={true} /> )}
            </div>
        );
    }
}

export default Deck;
