import Deck from './deck';
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return {
        cardsInDeck: state.cardsInDeck,
        showCardsOnHover: state.cardHoverVisible
    }
  }

export default connect(mapStateToProps)(Deck);