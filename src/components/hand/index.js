import { connect } from 'react-redux'
import Hand from './hand';

const mapStateToProps = state => {
    return {
        cardsInHand: state.cardsInHand
    }
  }

export default connect(mapStateToProps)(Hand);