import React, {Component} from 'react';
import Card from '../card';
import './hand.css';

/**
 * Player's hand holding the card
 */
class Hand extends Component {

    render() {
        const {cardsInHand} = this.props;
        return (
            <div className="hand">
                {cardsInHand.map((c,i) =>  <Card key={i} color={c.color} face={c.face} flipped={false} /> )}
            </div>
        );
    }

}

export default Hand;