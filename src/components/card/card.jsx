import React, { Component } from 'react';
import {Map} from 'immutable';
import './style.css';

class Card extends Component {
    // Unicode offset by color, simply add card value to it to get the character code
    UnicodeColorsOffset = Map({"clubs" : 0x1F0D0, "diamonds" : 0x1F0C0, "hearts" : 0x1F0B0, "spades": 0x1F0A0});

    render() {
        const {color, face, flipped} = this.props;

        let offset = this.UnicodeColorsOffset.get(color) + (face > 11 ? face + 1 : face);
        let character = "&#" + offset + ";";
        let classes = color + " card";
        if(flipped)
            classes += " flipped";

        return (
            <div className={classes}>
                <span className="front" dangerouslySetInnerHTML={{ __html: character}}></span>
                <span className="back">&#x1F0A0;</span>
            </div>
        );
    }
}

export default Card;