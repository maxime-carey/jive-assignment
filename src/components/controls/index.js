import { connect } from 'react-redux'
import Controls from './controls';
import Actions from '../../actions';

const mapStateToProps = state => ({
    cardHoverVisible: state.cardHoverVisible
});

const mapDispatchToProps = (dispatch) => ({
    deal: () => dispatch({ type: Actions.DEAL }),
    shuffle: () => dispatch({ type: Actions.SHUFFLE }),
    reset: () => dispatch({ type: Actions.RESET }),
    hoverChanged: (visible) =>  dispatch({ type: Actions.HOVERCHANGED, payload: visible }),
  });
  
  export default connect(mapStateToProps, mapDispatchToProps)(Controls);
  