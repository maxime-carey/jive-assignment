import React, {Component} from 'react';

/**
 * Controls: Controlbox for the game
 */
class Controls extends Component {

    constructor(props)
    {
        super(props);

        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
    }

    /**
     * handleCheckboxChange: handle the chkBoxShowCardsOnHover checkbox state change
     * @param {*} event 
     */
    handleCheckboxChange(event) {
        this.props.hoverChanged(event.target.checked);
    }

    render() {
        const {deal, shuffle, reset, cardHoverVisible} = this.props;

        return (
          <div className="control">
            <button onClick={deal}>Deal a Card</button>
            <button onClick={shuffle}>Shuffle Deck</button>
            <button onClick={reset}>Reset</button>
            <br />
            <input name="chkBoxShowCardsOnHover" 
                   type="Checkbox" 
                   checked={cardHoverVisible}
                   onChange={this.handleCheckboxChange} 
                />
            <label htmlFor="chkBoxShowCardsOnHover">Show card on hover</label>
          </div>
        );
    }

}

export default Controls;