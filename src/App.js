import React, { Component } from 'react';
import './App.css';
import {Deck, Hand, Controls} from './components';

/**
 * App: Our topmost component
 */
class App extends Component {
  render() {
    return (
      <div className="App">
        <Deck />
        <Controls />
        <Hand />
      </div>
      );
  }
}

export default App;