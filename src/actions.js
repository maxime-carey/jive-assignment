import { List } from 'immutable';

const SHUFFLE = 'SHUFFLE';
/**
 * Shuffle: Card shuffling action
 * @param state current redux state
 */
function Shuffle(state) {
  let cards = state.cardsInDeck;
  let card;

  // Pretty much brute force scrambling here, but mimick quite a lot what a human would do when shuffling a deck
  for (var i = 0; i < state.cardsInDeck.size * state.cardsInDeck.size; i++) {
    card = cards.last(); // Get a card
    cards = cards.pop(); // Get the collection without that last card
    cards = cards.insert(
      Math.round(Math.random() * state.cardsInDeck.size),
      card
    ); // Insert it back in a random place within the deck
  }

  return {
    ...state,
    cardsInDeck: cards
  };
}

const DEAL = 'DEAL';
/**
 * DealOneCard: Take the card from the top of the deck and place it in player's hand
 * @param {*} state current redux state
 */
function DealOneCard(state) {
  if (state.cardsInDeck.size > 0) {
    // Cannot deal if we don't have cards
    let card = state.cardsInDeck.first(); // Pick the first card
    card.flipped = false; // Show the card's front

    return {
      ...state,
      cardsInDeck: state.cardsInDeck.shift(), // Remove the card we picked and return the List
      cardsInHand: state.cardsInHand.push(card) // Add the card to the hand
    };
  } else {
    return state;
  }
}

const RESET = 'RESET';
/**
 * Reset: Reset the game's state to initial state
 */
function Reset(state) {
  let cards = [];
  const Colors = ['clubs', 'diamonds', 'hearts', 'spades'];

  // Generate each cards of the deck
  Colors.forEach(c => {
    for (var i = 1; i < 14; i++) {
      cards.push({
        color: c,
        face: i,
        flipped: true
      });
    }
  });

  return {
    ...state,
    cardsInHand: List(), // We start empty handed
    cardsInDeck: List(cards) // All the cards are in the deck
  };
}

const HOVERCHANGED = 'HOVERCHANGED';
/**
 * HoverChanged:
 * @param state Current redux store state
 * @param {boolean} visible
 */
function HoverChanged(state, visible) {
  return {
    ...state,
    cardHoverVisible: visible
  };
}

export default {
  SHUFFLE,
  Shuffle,
  DEAL,
  DealOneCard,
  RESET,
  Reset,
  HOVERCHANGED,
  HoverChanged
};
